import { todoService } from "@/services/todo.service";
import { getTodoByDate } from "./getTodoByDate";

export const removeTodo = (todo) => {
  return todoService
    .removeTodo(todo.id)
    .then(() => {
      return getTodoByDate(todo.userId, todo.createdDate);
    })
    .catch((error) => {
      console.log("error: ", error);
    });
};
