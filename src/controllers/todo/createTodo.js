import { todoService } from "@/services/todo.service";
import { getTodoByDate } from "./getTodoByDate";

export const createTodo = (todo) => {
  return todoService
    .createTodo(todo)
    .then(() => {
      return getTodoByDate(todo.userId, todo.createdDate);
    })
    .catch((error) => {
      console.log("error: ", error);
    });
};
