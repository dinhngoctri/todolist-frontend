import { todoService } from "@/services/todo.service";

export const getTodoByID = (todoID) => {
  return todoService
    .getTodoByID(todoID)
    .then((success) => {
      const todo = success.data;
      return todo.data;
    })
    .catch((error) => {
      console.log("error: ", error);
    });
};
