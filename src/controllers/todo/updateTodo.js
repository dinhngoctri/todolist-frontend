import { todoService } from "@/services/todo.service";
import { getTodoByDate } from "./getTodoByDate";

export const updateTodo = (todo) => {
  return todoService
    .updateTodo(todo)
    .then(() => {
      return getTodoByDate(todo.userId, todo.createdDate);
    })
    .catch((error) => {
      console.log("error: ", error);
    });
};
