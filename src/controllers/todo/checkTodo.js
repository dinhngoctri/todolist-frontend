import { todoService } from "@/services/todo.service";
import { getTodoByDate } from "./getTodoByDate";

export const checkTodo = (todo) => {
  return todoService
    .checkTodo(todo.id)
    .then(() => {
      return getTodoByDate(todo.userId, todo.createdDate);
    })
    .catch((error) => {
      console.log("error: ", error);
    });
};
