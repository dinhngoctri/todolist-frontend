import { todoService } from "@/services/todo.service";

export const getTodoByDate = (userId, createdDate) => {
  return todoService
    .getTodoByDate(userId, createdDate)
    .then((success) => {
      const todolist = success.data;
      return todolist.data;
    })
    .catch((error) => {
      console.log("error: ", error);
    });
};
