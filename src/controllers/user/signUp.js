import { userService } from "@/services/user.service";
import { signIn } from "./signIn";

export const signUp = async (userInfo) => {
  return userService
    .signUp(userInfo)
    .then(() => {
      return signIn(userInfo);
    })
    .catch((error) => {
      console.log("error: ", error);
    });
};
