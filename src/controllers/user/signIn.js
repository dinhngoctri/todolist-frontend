import { userService } from "@/services/user.service";

export const signIn = async (userInfo) => {
  return userService
    .signIn(userInfo)
    .then((success) => {
      const userInfo = success.data;
      sessionStorage.setItem("userInfo", JSON.stringify(userInfo.data));
      return userInfo.data;
    })
    .catch((error) => {
      console.log("error: ", error);
    });
};
