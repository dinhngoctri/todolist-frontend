import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";

import VCalendar from "v-calendar";
// Use v-calendar & v-date-picker components
Vue.use(VCalendar, {
  componentPrefix: "v", // Use <vc-calendar /> instead of <v-calendar />
});

import { IconsPlugin } from "bootstrap-vue";
Vue.use(IconsPlugin);

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");
