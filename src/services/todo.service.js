import { https } from "./configURL";

export const todoService = {
  createTodo: (todo) => {
    return https.post("/createTodo", todo);
  },
  getTodoByDate: (userId, createdDate) => {
    return https.get(`/getTodoByDate?userId=${userId}&createdDate=${createdDate}`);
  },
  getTodoByID: (todoID) => {
    return https.get(`/getTodoById/id-${todoID}`);
  },
  updateTodo: (todo) => {
    return https.patch("/updateContent", todo);
  },
  removeTodo: (todoID) => {
    return https.patch(`/removeTodo/todo-${todoID}`);
  },
  checkTodo: (todoID) => {
    return https.patch(`/toggleStatus/todo-${todoID}`);
  },
};
