import axios from "axios";

export const https = axios.create({
  baseURL: "http://localhost:4400/todolist-1.0",
  headers: { "Content-Type": "application/json" },
});
