import { https } from "./configURL";

export const userService = {
  signIn: (userInfo) => {
    return https.post("/signIn", userInfo);
  },
  signUp: (userInfo) => {
    return https.post("/signUp", userInfo);
  },
};
